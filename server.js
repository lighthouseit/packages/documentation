const express = require('express');
const path = require('path');
const { publicPath } = require('./config/config');

const app = express();

const PORT = process.env.PORT || 8000;

const dist = path.join(__dirname, './dist');

app.use(publicPath, express.static(dist));

app.get('*', (_, res) => res.sendFile(path.join(dist, './index.html')));

app.listen(PORT, () => console.log(`Server running on: ${PORT}`));
