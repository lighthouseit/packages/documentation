import React, { useEffect, useState } from 'react';
import {
  Divider,
  Table,
  Tag,
  Affix,
  Button,
  Input,
} from 'antd';
import { EditOutlined, CloseOutlined, SaveOutlined } from '@ant-design/icons';
import { connect } from 'umi';
import { Prism } from 'react-syntax-highlighter';
import Loading from '../../components/Loading';
import styles from './index.less';

const Library = ({
  library = {},
  loading,
  saving,
  match,
  dispatch,
}) => {
  const [editing, setEditing] = useState(false);

  const { id } = match.params;

  useEffect(() => {
    dispatch({ type: 'library/getLibrary', id });
  }, [dispatch]);

  const reduceLibrary = (current) => dispatch({
    type: 'library/reduce',
    payload: {
      current: {
        ...library,
        ...current,
      },
    },
  });

  const {
    version,
    name,
    description,
    installation,
    usage = {},
    examples = [],
    references = [],
  } = library || {};

  const renderVersion = () => {
    if (editing) {
      return (
        <Input
          style={{ width: 80 }}
          value={version}
          onChange={(e) => reduceLibrary({ version: e.target.value })}
        />
      );
    }

    return <h3>{version}</h3>;
  };

  const renderHeader = () => (
    <div className={styles.header}>
      {renderVersion()}

      <h1>{name}</h1>

      <Affix offsetTop={88}>
        {editing ? (
          <>
            <Button
              style={{ marginRight: 8 }}
              type="danger"
              icon={<CloseOutlined />}
              onClick={() => setEditing(false)}
            >
              Cancelar
            </Button>

            <Button
              type="primary"
              icon={<SaveOutlined />}
              onClick={async () => {
                await dispatch({ type: 'library/updateLibrary', id });

                setEditing(false);
              }}
              loading={saving}
            >
              Salvar
            </Button>
          </>
        ) : (
          <Button
            type="primary"
            icon={<EditOutlined />}
            onClick={() => setEditing(true)}
          >
            Editar
          </Button>
        )}
      </Affix>
    </div>
  );

  const renderDescription = () => {
    if (editing) {
      return (
        <Input.TextArea
          value={description}
          rows={6}
          onChange={(e) => reduceLibrary({ description: e.target.value })}
        />
      );
    }

    return <p className={styles.pre}>{description}</p>;
  };

  const renderInstalation = () => {
    if (editing) {
      return (
        <Input.TextArea
          value={installation}
          rows={4}
          onChange={(e) => reduceLibrary({ installation: e.target.value })}
        />
      );
    }

    return <p className={styles.pre}>{installation}</p>;
  };

  const renderUsage = () => {
    const { language, code } = usage;

    if (editing) {
      return (
        <>
          <Input
            value={language}
            onChange={(e) => reduceLibrary({
              usage: {
                code,
                language: e.target.value,
              },
            })}
          />

          <br />

          <Input.TextArea
            value={code}
            rows={12}
            onChange={(e) => reduceLibrary({
              usage: {
                language,
                code: e.target.value,
              },
            })}
          />
        </>
      );
    }

    return (
      <Prism language={language}>
        {code}
      </Prism>
    );
  };

  const renderExample = ({
    language,
    code,
    api = [],
    images = [],
  }, i) => {
    if (editing) {
      return (
        <div key={`example-${i}`}>
          <Input
            value={language}
            onChange={(e) => {
              const newexamples = [].concat(examples);

              newexamples[i] = {
                code,
                api,
                language: e.target.value,
              };

              reduceLibrary({ examples: newexamples });
            }}
          />

          <br />

          <Input.TextArea
            value={code}
            rows={12}
            onChange={(e) => {
              const newexamples = [].concat(examples);

              newexamples[i] = {
                language,
                api,
                code: e.target.value,
              };

              reduceLibrary({ examples: newexamples });
            }}
          />

          <Divider type="horizontal" />
        </div>
      );
    }

    return (
      <div key={`example-${i}`}>
        <div className={styles.exampleImages}>
          {images.map((image, j) => (
            <div key={`example-image-${j}`} className={styles.exampleImage}>
              <img src={image} alt="Example" />
            </div>
          ))}
        </div>

        <Prism language={language}>
          {code}
        </Prism>

        <Table
          dataSource={api}
          columns={[
            {
              title: 'Property',
              dataIndex: 'property',
              key: 'property',
              render: (text) => <b>{text}</b>,
            },
            {
              title: 'Description',
              dataIndex: 'description',
              key: 'description',
            },
            {
              title: 'Type',
              dataIndex: 'type',
              key: 'type',
              render: (text) => (
                <Tag color="volcano">
                  {text}
                </Tag>
              ),
            },
            {
              title: 'Default',
              dataIndex: 'default',
              key: 'default',
              render: (text) => (
                <Tag color="geekblue">
                  {text}
                </Tag>
              ),
            },
          ]}
          rowKey="property"
          pagination={false}
        />

        <Divider type="horizontal" />
      </div>
    );
  };

  const renderReference = (link, i) => {
    if (editing) {
      return (
        <div key={`link-${i}`}>
          <Input
            value={link}
            onChange={(e) => {
              const newreferences = [].concat(references);

              newreferences[i] = e.target.value;

              reduceLibrary({ references: newreferences });
            }}
          />

          <br />
        </div>
      );
    }

    return (
      <div key={`link-${i}`}>
        <a href={link} target="_blank" rel="noopener noreferrer">
          {link}
        </a>

        <br />
      </div>
    );
  };

  return (
    <Loading loading={loading}>
      {renderHeader()}

      <Divider type="horizontal" />

      <h3>Descrição</h3>

      {renderDescription()}

      <Divider type="horizontal" />

      <h3>Instalação</h3>

      {renderInstalation()}

      <Divider type="horizontal" />

      <h3>Uso</h3>

      {renderUsage()}

      <Divider type="horizontal" />

      <h3>Exemplos</h3>

      {examples.map(renderExample)}

      <h3>Referências</h3>

      {references.map(renderReference)}
    </Loading>
  );
};

export default connect(({ library, loading }) => ({
  library: library.current,
  loading: loading.effects['library/getLibrary'],
  saving: loading.effects['library/updateLibrary'],
}))(Library);
