import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  List,
  Button,
  Modal,
  Input,
  Typography,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { Link, connect } from 'umi';
import Loading from '../../components/Loading';
import styles from './index.less';

const Libraries = ({ libraries, loading, dispatch }) => {
  useEffect(() => {
    dispatch({ type: 'library/getLibraries' });
  }, [dispatch]);

  const [visibleAddLibrary, setVisibleAddLibrary] = useState(false);

  const [addLibraryVersion, setAddLibraryVersion] = useState('');

  const [addLibraryName, setAddLibraryName] = useState('');

  const addLibrary = async () => {
    if (addLibraryVersion && addLibraryName) {
      setVisibleAddLibrary(false);

      await dispatch({
        type: 'library/createLibrary',
        version: addLibraryVersion,
        name: addLibraryName,
      });
    }
  };

  return (
    <Loading loading={loading}>
      <List
        header={(
          <div className={styles.listHeader}>
            <h4 className={styles.listHeaderTitle}>Libraries</h4>

            <Button
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => setVisibleAddLibrary(true)}
            >
              Adicionar
            </Button>
          </div>
        )}
        dataSource={libraries}
        renderItem={({ id, version, name }) => (
          <Link to={`/library/${id}`}>
            <List.Item>
              <Typography.Text style={{ marginRight: 8 }} code>
                {version}
              </Typography.Text>

              {name}
            </List.Item>
          </Link>
        )}
        bordered
      />

      <Modal
        title="Adicionar Biblioteca"
        visible={visibleAddLibrary}
        onOk={addLibrary}
        onCancel={() => setVisibleAddLibrary(false)}
      >
        <Row gutter={[8, 8]}>
          <Col span={6}>
            <Input
              placeholder="Version"
              value={addLibraryVersion}
              onChange={(e) => setAddLibraryVersion(e.target.value)}
            />
          </Col>

          <Col span={18}>
            <Input
              placeholder="Name"
              value={addLibraryName}
              onChange={(e) => setAddLibraryName(e.target.value)}
            />
          </Col>
        </Row>
      </Modal>
    </Loading>
  );
};

export default connect(({ library, loading }) => ({
  libraries: library.libraries,
  loading: loading.effects['library/getLibraries'],
}))(Libraries);
