import React, { useEffect, useState } from 'react';
import ProLayout, { DefaultFooter } from '@ant-design/pro-layout';
import { Link } from 'umi';
import firebase from 'firebase';
import 'firebase/analytics';
import 'firebase/auth';
import brand from '../../assets/brand.png';
import styles from './index.less';

const App = ({ children, ...props }) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    firebase.initializeApp({
      apiKey: 'AIzaSyAZd_OA1cLpXGJZOxwU_ku16edRvqga8YI',
      authDomain: 'documentation-b6afa.firebaseapp.com',
      databaseURL: 'https://documentation-b6afa.firebaseio.com',
      projectId: 'documentation-b6afa',
      storageBucket: 'documentation-b6afa.appspot.com',
      messagingSenderId: '108360556295',
      appId: '1:108360556295:web:cd45d67638dc447f8a6fa2',
      measurementId: 'G-GJMNF1SWYV',
    });

    firebase.analytics();

    firebase.auth().signInAnonymously()
      .then(() => setLoading(false))
      .catch((err) => console.log(err));
  }, []);

  const menuHeaderRender = (logoChildren, titleChildren) => (
    <Link className={styles.menuItem} to="/">
      {logoChildren}

      {titleChildren}
    </Link>
  );

  const menuItemRender = ({ path = '/' }, itemChildren) => (
    <Link className={styles.menuItem} to={path}>
      {itemChildren}
    </Link>
  );

  // const rightContentRender = () => (
  //   <span style={{ color: 'white', marginLeft: 12, marginRight: 12 }}>
  //     Alvaro Silveira
  //   </span>
  // );

  const footerRender = () => (
    <DefaultFooter
      links={[
        {
          key: 'lighthouseit',
          title: 'Lighthouseit',
          href: 'https://lighthouseit.com.br',
          blankTarget: true,
        },
      ]}
      copyright="2020"
    />
  );

  return (
    <ProLayout
      {...props}
      logo={brand}
      title="Documentation"
      layout="topmenu"
      menuHeaderRender={menuHeaderRender}
      menuItemRender={menuItemRender}
      // rightContentRender={rightContentRender}
      footerRender={footerRender}
      loading={loading}
      fixedHeader
    >
      <div className={styles.children}>
        {children}
      </div>
    </ProLayout>
  );
};

export default App;
