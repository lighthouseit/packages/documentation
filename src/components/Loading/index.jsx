import React from 'react';
import { Spin } from 'antd';
import styles from './index.less';

const Loading = ({ loading, children }) => {
  if (typeof loading === 'undefined' || loading) {
    return (
      <div className={styles.loader}>
        <Spin size="large" />
      </div>
    );
  }

  return (
    <div className={styles.screen}>
      {children}
    </div>
  );
};

export default Loading;
