import Request from '@lighthouseapps/request/lib/Request';

const documentation = new Request();

documentation.initialize(
  {
    getLibraries: {
      method: 'GET',
      uri: '/v1/libraries',
    },
    getLibrary: {
      method: 'GET',
      uri: '/v1/library/:name',
    },
  },
  {
    baseURL: 'http://localhost:8000',
    timeout: 10000,
    onHeaders: {},
  },
);

export default documentation;
