import { message } from 'antd';
import firebase from 'firebase';
import 'firebase/firestore';

export default {
  namespace: 'library',
  state: {
    current: undefined,
    libraries: [],
  },
  effects: {
    * getLibraries(_, { call, put }) {
      const db = firebase.firestore().collection('packages');

      const getLibraries = db
        .get()
        .then((snapshot) => snapshot.docs.map((doc) => ({
          ...doc.data(),
          id: doc.id,
        })))
        .catch((err) => {
          console.log(err);

          message.error('Não foi possível carregar as bibliotecas');
        });

      const libraries = yield call(() => getLibraries);

      if (!libraries) return;

      yield put({
        type: 'reduce',
        payload: {
          libraries,
        },
      });
    },
    * getLibrary({ id }, { call, put }) {
      const db = firebase.firestore().collection('packages');

      const getLibrary = db
        .doc(id)
        .get()
        .then((doc) => doc.data())
        .catch((err) => {
          console.log(err);

          message.error('Não foi possível carregar a biblioteca');
        });

      const current = yield call(() => getLibrary);

      if (!current) return;

      yield put({
        type: 'reduce',
        payload: {
          current,
        },
      });
    },
    * createLibrary({ version, name }, { call, put }) {
      const db = firebase.firestore().collection('packages');

      const createLibrary = db
        .doc()
        .set({ version, name })
        .catch((err) => {
          console.log(err);

          message.error('Não foi possível criar a biblioteca');
        });

      yield call(() => createLibrary);

      yield put({ type: 'getLibraries' });
    },
    * updateLibrary({ id }, { select, call }) {
      const current = yield select((state) => state.library.current);

      const db = firebase.firestore().collection('packages');

      const updateLibrary = db
        .doc(id)
        .set(current)
        .catch((err) => {
          console.log(err);

          message.error('Não foi possível salvar as alterações da biblioteca');
        });

      yield call(() => updateLibrary);
    },
  },
  reducers: {
    reduce(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
