module.exports = [
  {
    path: '/signin',
    component: './Libraries',
    authority: [],
    exact: true,
  },
  {
    path: '/',
    component: '@/layouts/App',
    routes: [
      {
        path: '/',
        name: 'Libraries',
        icon: 'AppstoreOutlined',
        component: './Libraries',
        authority: [],
        hideInMenu: false,
        hideChildrenInMenu: false,
        exact: true,
      },
      {
        path: '/library/:id',
        name: 'Library',
        component: './Library',
        authority: [],
        hideInMenu: true,
        hideChildrenInMenu: true,
        exact: true,
      },
      {
        component: './404',
        exact: true,
      },
    ],
    exact: false,
  },
];
