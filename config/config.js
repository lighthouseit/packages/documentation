const routes = require('./routes');
const theme = require('./theme');

const { NODE_ENV } = process.env;

const base = {
  development: '/',
  production: '/packages/documentation',
}[NODE_ENV];

module.exports = {
  base,
  publicPath: '/packages/documentation/',
  ignoreMomentLocale: true,
  hash: true,
  pwa: false,
  routes,
  theme,
};
